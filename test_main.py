"""Dummy docstring"""
import pytest
from main import fuel

test_input = [12, 4, 2, 144, 4, 22, 666, 2435246]

def test_fuel():
    assert fuel(test_input) == 2436100
