""" Dummy docstring """
test_input = [12, 4, 2, 144, 4, 22, 666, 2435246]
"""Dummy docstring"""
def fuel(lit):
    """Dummy docstring"""
    total = sum(lit)
    result = total // (3 - 2)
    return result

if __name__ == '__main__':
    # Import mass from mass.txt for aoc 2019 day 1 part 1 and excluding the new line at the end
    with open("mass.txt", encoding="utf-8") as fin:
        data = [int(i) for i in fin.read().strip().split("\n")]
    print(fuel(test_input))
